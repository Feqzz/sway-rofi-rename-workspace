#!/usr/bin/env bash

new_workspace_name=$(rofi -dmenu -p "New name: ")
current_workspace_name=$(swaymsg -t get_workspaces | jq -r '.[] | select(.focused==true) | .name')

swaymsg rename workspace $current_workspace_name to $new_workspace_name
